#!/bin/sh
#    portprober.sh - Check ports on different hosts
#    Copyright (C) 2022 -fab- <fab@foobucket.xyz>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

display_usage () {
    printf "Usage: %s <filename>\n" "${0}"
    printf "\nfilename must be a csv formatted file.\n"
}

# execute ping and netcat commands for all ips and ports
# and return list
get_ping_and_ports_list () {
	while IFS="|" read -r name ip ports; do
		savIFS=${IFS}
		if ! ping -c 1 "${ip}" 1> /dev/null 2>&1; then
            printf "PING (%s) %s - FAIL\n" "${name}" "${ip}"
		else
			printf "PING (%s) %s - PASS\n" "${name}" "${ip}"
			IFS=,; set -f; set -- ${ports}
			for port
			do
				ncat -w 1 -z "${ip}" "${port}" 1>/dev/null 2>&1
				case ${?} in
					0 ) status="PASS" ;;
					* ) status="FAIL" ;;
				esac
				printf "PORT (%s) %s - Port: %s - %s\n" \
                    "${name}" "${ip}" "${port}" "${status}"
			done
		fi
		IFS=${savIFS}
	done < "${1}"
}

# Check usage
if [ ${#} -ne 1 ]; then
    printf "ERROR: Invalid number of arguments!\n"
    display_usage
    exit 1
fi

# CSV filename provided on commandline
LIST_FILE="${1}"

if [ ! -f "${LIST_FILE}" ]; then
    printf "ERROR: \"%s\" not found!\n" "${LIST_FILE}"
    display_usage
    exit 2
fi

# Stores "PASS" or "FAIL" of complete operation
COMPLETE="PASS"

# Extract names from csv file
NAMES="$(sed -E 's/^([^|]*)\|.*$/\1/' "${LIST_FILE}" | sort -u | tr '\n' ' ')"

# get results from PING and PORTS
RESULT=$( get_ping_and_ports_list "${LIST_FILE}" )

# Store complete output
OUTPUT=""

IFS=' '
for name in ${NAMES}; do
	list_name=$(printf "%s\n" "${RESULT}" | grep "${name}")
	printf "%s\n" "${list_name}" | grep 'FAIL' > /dev/null 2>&1
	case $? in
		0 ) OUTPUT="${OUTPUT}[ \033[0;31m-FAIL-\033[1;39m ] ${name}\n"; COMPLETE="FAIL" ;;
		* ) OUTPUT="${OUTPUT}[ \033[1;32mONLINE\033[1;39m ] ${name}\n" ;;
	esac
done

# Prepare and do output

# clear screen
clear

[ "${COMPLETE}" = "PASS" ] && printf "\033[0;32m" || printf "\033[0;31m"
printf "%s" "${COMPLETE}" | figlet -t -c
printf "\033[1;39m"
printf "%b" "${OUTPUT}"

# portprober

Bash Script to check if ports are open on some hosts. May also run on other
shells (I know of ash and mksh).

## Dependencies

* ping: (should come with the linux system)
* nmap (ncat): needed to check for open ports
* figlet: for some fancy output of result
* sed/grep/sort/tr

## Usage

To run the command with the csv formatted ports file:

    $ ./portprober.sh ports.csv

## ports list file

The ports list file is a csv formatted file with one IP-Address on each line
and separated by "|":

    servername|IPv4 or IPv6|comma separated ports list

For example:

    scanme.org|45.33.32.156|22,80,9929,31337
    scanme.org|2600:3c01::f03c:91ff:fe18:bb2f|22,80,31337
    foobucket.xyz|185.232.71.132|80,443,1965
    foobucket.xyz|2a03:4000:4e:e28:38ac:4fff:fe98:7018|80,443,1965

You can add as many address lines as you wish. The output is grouped by the
servername. It's simply a name, not a FQDN, as portprober only depends on 
the IPv4 or IPv6 address.

Look at the example ports.csv file
